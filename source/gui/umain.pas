unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ActnList, StdCtrls,
  uAppVer, uAppLog, uAppUp, uLog;

type

  { TFormMain }

  TFormMain = class(TForm)
    actCheckForUpdate: TAction;
    Button1: TButton;
    MainActList: TActionList;
    procedure actCheckForUpdateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

{ TFormMain }

procedure TFormMain.FormCreate(Sender: TObject);
begin
  {$ifopt D+}
    DebugLogging := BUILD_DEBUG;
  {$endif}
end;

procedure TFormMain.actCheckForUpdateExecute(Sender: TObject);
var
  Latest : TAppUpdateInfoType;
begin
   CheckForUpdate(Latest);
end;

procedure TFormMain.FormShow(Sender: TObject);
begin
  FormLog.Show;
  LogToFile('output.log');
end;

end.

