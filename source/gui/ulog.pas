unit uLog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  uAppLog;

type

  { TFormLog }

  TFormLog = class(TForm)
    MemoLog: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
  public
     procedure DoOnAppLogMessage(AMessage : String);
  end;

var
  FormLog: TFormLog;

implementation


{$R *.lfm}

{ TFormLog }

procedure TFormLog.FormCreate(Sender: TObject);
begin
   LogPause;
   MemoLog.Lines.AddStrings(TextLog, True);
   RegisterAppLogMessageHandler(@DoOnAppLogMessage);
   LogResume;
end;

procedure TFormLog.FormDestroy(Sender: TObject);
begin
  LogPause;
  UnregisterAppLogMessageHandler(@DoOnAppLogMessage);
  LogResume;
end;

procedure TFormLog.DoOnAppLogMessage(AMessage: String);
begin
  MemoLog.Lines.Add(AMessage);
end;

end.

