// Application, FPC & Lazarus Version Information

const
{$IF declared(TAppVersionInfoType)}

  VersionInfo : TAppVersionInfoType = (
  { The default Free Pascal Compiler }
    FreePascal : (
      Version : '3.3.1';
      Revision : '47424';
      Target : 'x86_64';
    );
  { The Lazarus I.D.E }
    Lazarus : (
      Version : '2.1.0';
      Revision : '64142';
    );
  { Source & Subversion Last Changed Commit }
    Source : (
      Version : '3.0.0.16';
      Revision : '27';
      Online : '';
      URL : 'http://gitlab.com/FreeDOS/fdrepo';
      Commit : '5422ceb3a376ba0d3156651bb4f26c8a82d684ef';
    );
  { Version Build Atributes } 
    Attributes : ( 
      Debug : True;
      PreRelease : True;
      Patched : False;
      PrivateBuild : False;
      SpecialBuild : False;
      Language : '';
      CharSet : '';
      Date : '2020-12-22 15:48:10';
    );
  { General Application Information }
    Application : (
      Identifier : 'com.shidel.fdrepogui';
      Version : '3.0.0.16';
      Comments : '';
      CompanyName : 'Shidel';
      FileDescription : 'FreeDOS Software Repository Manager GUI';
      InternalName : 'fdrepoGUI';
      LegalCopyright : 'Copyright (c) 2020 Jerome Shidel';
      LegalTrademarks : '';
      OriginalFilename : 'fdrepoGUI';
      ProductName : 'fdrepoGUI';
      ProductVersion : '3.x';
      Year : '2020';
    );
  );

{$ELSE}

  { The default Free Pascal Compiler }
  FPC_VERSION='3.3.1';
  FPC_REVISION='47424';
  FPC_TARGET='x86_64';

  { Platform specific and cross-compilers }
  FPC_PPC386='43302';
  FPC_PPCX64='47424';

  { The Lazarus I.D.E }
  LAZARUS_VERSION='2.1.0';
  LAZARUS_REVISION='64142';

  { Source & Subversion Last Changed Commit }
  SOURCE_VERSION='3.0.0.16';
  SOURCE_REVISION='27';
  SOURCE_URL='http://gitlab.com/FreeDOS/fdrepo';
  SOURCE_ONLINE='';
  SOURCE_COMMIT='5422ceb3a376ba0d3156651bb4f26c8a82d684ef';

  { Version Build Atributes } 
  BUILD_DEBUG=True;
  BUILD_PRERELEASE=True;
  BUILD_PATCHED=False;
  BUILD_PRIVATE=False;
  BUILD_SPECIAL=False;
  BUILD_LANGUAGE='';
  BUILD_CHARSET='';
  BUILD_DATE='2020-12-22 15:48:10';

  { General Application Information }
  APP_IDENTIFIER='com.shidel.fdrepogui';
  APP_VERSION='3.0.0.16';
  APP_COMPANYNAME='Shidel';
  APP_FILEDESCRIPTION='FreeDOS Software Repository Manager GUI';
  APP_INTERNALNAME='fdrepoGUI';
  APP_LEGALCOPYRIGHT='Copyright (c) 2020 Jerome Shidel';
  APP_ORIGINALFILENAME='fdrepoGUI';
  APP_PRODUCTNAME='fdrepoGUI';
  APP_PRODUCTVERSION='3.x';
  APP_YEAR='2020';

{$ENDIF}
