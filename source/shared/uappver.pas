unit uAppVer;

{$mode objfpc}{$H+}

interface
{*
type
  TAppVersionInfoType = record
{ The default Free Pascal Compiler }
  FreePascal : record
    Version : String;
    Revision : String;
    Target : String;
  end;
{ The Lazarus I.D.E }
  Lazarus : record
    Version : String;
    Revision : String;
  end;
{ Source & Subversion Last Changed Commit }
  Source : record
    Version : String;
    Revision : String;
    Online : String;
    URL : String;
    Commit : String;
  end;
{ Version Build Atributes }
  Attributes : record
    Debug : Boolean;
    PreRelease : Boolean;
    Patched : Boolean;
    PrivateBuild : Boolean;
    SpecialBuild : Boolean;
    Language : String;
    CharSet : String;
    Date : String;
  end;
{ General Application Information }
  Application : record
    Identifier : String;
    Version : String;
    Comments : String;
    CompanyName : String;
    FileDescription : String;
    InternalName : String;
    LegalCopyright : String;
    LegalTrademarks : String;
    OriginalFilename : String;
    ProductName : String;
    ProductVersion : String;
    Year : String;
  end;
end;
*}

{$I version.inc}

implementation

end.

