program fdrepoServer;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp
  { you can add units after this },
  uAppVer;

type

  { TFDRepoServer }

  TFDRepoServer = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
    procedure WriteVersion; virtual;
  end;

{ TFDRepoServer }

procedure TFDRepoServer.DoRun;
var
  ErrorMsg: String;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('hqv', ['help', 'quite', 'verbose']);
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }

  // stop program loop
  Terminate;
end;

constructor TFDRepoServer.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TFDRepoServer.Destroy;
begin
  inherited Destroy;
end;

procedure TFDRepoServer.WriteHelp;
begin
  { add your help code here }
  WriteVersion;
  WriteLn;
  writeLn('Usage: ', ExtractFileName(ExeName), ' [options]');
  WriteLn;
  WriteLn(#9,'-h, --help', #9, 'Display this help text');
  WriteLn;
  WriteLn(#9,'-v, --verbose', #9, 'Increase program text display one notch');
  WriteLn(#9,'-q, --quite', #9, 'Suppress nearly all text display');
end;

procedure TFDRepoServer.WriteVersion;
begin
  WriteLn(APP_PRODUCTNAME, ' version ', APP_VERSION);
end;

var
  Application : TFDRepoServer;

{$R *.res}


begin
  Application:=TFDRepoServer.Create(nil);
  Application.Title:=APP_PRODUCTNAME;;
  Application.Run;
  Application.Free;
end.

